# Overview

## Karate

### In a nutshell

Karate is an integration testing tool similar to Cucumber. Features are written in natural language/Gherkin.

### Benefits

* Karate doesn't need any Java code
* JSON is a first-class citizen
* More complex tests can use javascript
* Supports XML out-of-the-box
* Native YAML support
* Gradle/Maven familiar structure
* Parallel feature/scenario execution
* Authentication and Header management
* Retry support
* Multi-environment support
* JSON schema support

### Drawbacks

* Standalone version is pretty basic
    - Config support?
    - Logging support?

## Simple demo

* See src/test/java 
