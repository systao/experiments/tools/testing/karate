package fr.systao.karate.testing;

import org.junit.runner.RunWith;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit4.Karate;

@KarateOptions(
		features = "classpath:features", 
		tags = "~@ignore") 
@RunWith(Karate.class)
public class KarateTests {
	
}
