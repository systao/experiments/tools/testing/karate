Feature: Testing Karate Integration testing on Ghibli public API

Background: 
* url 'https://ghibliapi.herokuapp.com'

Scenario: Retrieve list of films

Given   path 'films'
When    method get
Then    status 200
And 		match $ == '#array'      
        * assert response.length > 0
        * def film = karate.filter(response, function(f) { return f.title.toLowerCase() == 'castle in the sky' })[0]
        * assert film != null

Given   path 'films', film.id
When    method get
Then    status 200
        * match $ contains { release_date: "1986" }
